// int WINAPI MessageBox(HWND hWnd 窗口句柄, LPCTSTR lpText 字符串描述, LPCTSTR
// lpCaption 字符串标题, UINT uType 窗口类型); MessageBoxA(0, "厉不厉害你坤哥",
// "鸡你太美", 1); DWORD 四字节无符号整数 pid 进程 tid 线程

#include <Windows.h>
#include <cstdio>
#include <ios>
#include <iostream>

#include "Data.h"

// #define 基地址 0x401000

void CmdWindow();

int main()
{
    UINT_PTR sunLight = Read(Read(0x400000 + 0x355E0C) + 0x868) + 0x5578;
    Write(sunLight, 9000);
    std::cout << std::hex << Read(0x401000) << std::endl;
    CmdWindow();
    return 0;
}


void CmdWindow()
{
    std::cout << "Fakebi v0.0.0" << std::endl
              << " /$$$$$$$$       /$$                 /$$       /$$" << std::endl
              << "| $$_____/      | $$                | $$      |__/" << std::endl
              << "| $$    /$$$$$$ | $$   /$$  /$$$$$$ | $$$$$$$  /$$" << std::endl
              << "| $$$$$|____  $$| $$  /$$/ /$$__  $$| $$__  $$| $$" << std::endl
              << "| $$__/ /$$$$$$$| $$$$$$/ | $$$$$$$$| $$  \\ $$| $$" << std::endl
              << "| $$   /$$__  $$| $$_  $$ | $$_____/| $$  | $$| $$" << std::endl
              << "| $$  |  $$$$$$$| $$ \\  $$|  $$$$$$$| $$$$$$$/| $$" << std::endl
              << R"(|__/   \_______/|__/  \__/ \_______/|_______/ |__/)" << std::endl
              << "                                YuanShen version 3.8" << std::endl;
}
