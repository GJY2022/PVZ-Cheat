#include <Windows.h>
#include <basetsd.h>
#include <iostream>
#include <memory>

#include "Data.h"

// 窗口句柄
HWND WindowHandle()
{
    return FindWindowA("MainWindow", "Plants vs. Zombies");
}

// 进程句柄
HANDLE ProcessHandle()
{
    return OpenProcess(PROCESS_ALL_ACCESS, FALSE, ID(true));   // 参1为权限
}

DWORD ID(bool idValue)
{
    DWORD processID = 0;   // 进程
    DWORD threadID = 0;    // 线程

    threadID = GetWindowThreadProcessId(WindowHandle(), &processID);

    if (idValue) {
        return processID;
    }
    if (!idValue) {
        return threadID;
    }

    return 0;
}

UINT_PTR Read(UINT_PTR pData)
{
    // LPVOID returnValue = nullptr;
    UINT_PTR returnValue = 0;
    ReadProcessMemory(
        ProcessHandle(), reinterpret_cast<LPCVOID>(pData), &returnValue, sizeof(int), nullptr);

    CloseHandle(ProcessHandle());

    return returnValue;
}
// reinterpret_cast;
int Write(UINT_PTR pData, UINT_PTR pNewData)
{

    WriteProcessMemory(
        ProcessHandle(), reinterpret_cast<LPVOID>(pData), &pNewData, sizeof(int), nullptr);

    CloseHandle(ProcessHandle());   // 关闭线程句柄

    return 0;
}