﻿#ifndef _DATA_H
#define _DATA_H

#include <Windows.h>
#include <iostream>

HWND   WindowHandle();
HANDLE ProcessHandle();
DWORD  ID(bool idValue);

UINT_PTR Read(UINT_PTR pData);

int Write(UINT_PTR pData, UINT_PTR pNewData);

#endif